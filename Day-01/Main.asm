[DEFAULT REL]
[BITS 64]


extern printf
extern sscanf
extern memset
extern DestroyObject
extern PostQuitMessage
extern OpenFile
extern CreateVector
extern VectorAt
extern VectorSize
extern VectorPushBack
extern DestroyVector
extern ReadLineFromFile


global main


segment .data 
__data_start:
lpszFile	db	"./Input/Day-01-00.txt",0x00
hFile		dq	0
hvData		dq	0
lValueRead	dd	0

csBuffer	times 512 db 0

lpszScanNum	db 	"%d",0x0A,0x00
lpszPrintLine	db	"%s",0x0A,0x00
lpszPrintNum	db	"%d",0x0A,0x00
lpszAnswer1	db	"Value 1: %ld",0x0A,0x00



segment .text	

main:
	push rbp,
	mov rbp, rsp

	; Open the file
	lea rdi, [lpszFile]
	mov rsi, 1 ; FILE_PERMISSION_READ
	xor rdx, rdx
	sub rsp, 32
	call OpenFile wrt ..plt
	add rsp, 32
	mov qword [hFile], rax

	; CreateVector
	mov rdi, 200h
	mov rsi, 4,
	xor rdx, rdx 
	sub rsp, 32
	call CreateVector wrt ..plt 
	add rsp, 32 
	mov qword [hvData], rax 



	__ReadTheFile:
	; Call ReadLineFromFile
	mov rdi, qword [hFile]
	lea rsi, [csBuffer]
	mov rdx, 511,
	mov rcx, 4 ; FILE_READ_SINGLE_POINTER_SAFE
	sub rsp, 32
	call ReadLineFromFile wrt ..plt 
	add rsp, 32
	mov r14, rax ; Size read


	; Sscanf and store
	lea rdi, [csBuffer]
	lea rsi, [lpszScanNum]
	lea rdx, [lValueRead]
	sub rsp, 32
	call sscanf wrt ..plt
	add rsp, 32

	; Push back to vector
	mov rdi, qword [hvData]
	lea rsi, [lValueRead]
	xor rdx, rdx
	sub rsp, 24
	call VectorPushBack wrt ..plt 
	add rsp, 24

	; Clear memory
	lea rdi, [csBuffer]
	mov esi, 0x00
	mov rdx, 512
	sub rsp, 24
	call memset wrt ..plt 
	add rsp, 24

	; Compare bytes read with 0, if we read more than 0, then
	; read some more. 
	cmp r14, 0
	jg __ReadTheFile


	; At this point, we have populated hvData with the full
	; contents of the file

	

	; Get VectorSize
	mov rdi, qword [hvData]
	sub rsp, 8
	call VectorSize wrt ..plt 
	add rsp, 8
	
	; These two will consistently be compared.
	mov r14, rax ; Stores vector size
	mov r15, 0 ; r15 can be index iterator

	__FirstLoop:		
	mov rdi, qword [hvData]
	mov rsi, r15 
	sub rsp, 16
	call VectorAt wrt ..plt 
	add rsp, 16

	mov rbx, rax ; lValueX 
	xor r12, r12 ; Second Iterator
		__SecondLoop:
		mov rdi, qword [hvData]
		mov rsi, r12 

		mov rdi, qword [hvData]
		mov rsi, r12 
		sub rsp, 16 
		call VectorAt wrt ..plt 
		add rsp, 16 
		
		xor rcx, rcx 
		add ecx, dword [rax] ; Output of Vector Y
		add ecx, dword [rbx] ; Output of Vector X

		cmp ecx, 2020
		jne __SecondLoopNotEqual

		__SecondLoopEqual:
		mov ecx, dword [rax] 
		mov eax, dword [rbx]
		imul ecx, eax 
		lea rdi, [lpszAnswer1],
		xor rsi, rsi 
		mov esi, ecx
		sub rsp, 16
		call printf wrt ..plt 
		add rsp, 16
		jmp __Found
		
		__SecondLoopNotEqual:
		inc r12
		cmp r12, r14
		jle __SecondLoop


	; TODO FINISH

	inc r15 
	cmp r15, r14
	jle __FirstLoop


	__Found:
	; Done


	; DestroyVector
	mov rdi, qword [hvData]
	sub rsp, 16
	call DestroyVector wrt ..plt 
	add rsp, 16 

	; Close the file
	mov rdi, qword [hFile]
	sub rsp, 16
	call DestroyObject wrt ..plt
	add rsp, 16

	ret
