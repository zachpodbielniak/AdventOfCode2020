#include <PodNet/PodNet.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CContainers/CVector.h>


#define INPUT_FILE "./Input/Day-01-00.txt"


LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hFile;
	HVECTOR hvData;
	UARCHLONG ualIndex;
	UARCHLONG ualJndex;
	UARCHLONG ualKndex;
	LONG lValueX;
	LONG lValueY;
	LONG lValueZ;
	LONGLONG llValueProduct;
	LONG lValueRead;
	CSTRING csBuffer[1024];
	LPSTR lpszBuffer;
	BOOL bFound1;
	BOOL bFound2;

	
	/* Open File */
	hFile = OpenFile(INPUT_FILE, FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == hFile)
	{ PostQuitMessage(1); }

	/* Create Vector */
	hvData = CreateVector(0x200, sizeof(LONG), 0);
	if (NULLPTR == hvData)
	{ PostQuitMessage(2); }

	/* For Address Of Access */
	lpszBuffer = (LPSTR)csBuffer;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	bFound1 = FALSE;
	bFound2 = FALSE;

	/* Read the file line by line, store output into the vector for later use */
	while (0 < ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	)){
		lValueRead = 0;
		StringScanFormat(lpszBuffer, "%d\n", &lValueRead);

		VectorPushBack(hvData, &lValueRead, 0);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	/* First Loop */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){ 
		lValueX = *(LPLONG)VectorAt(hvData, ualIndex);

		/* Second Loop */
		for (
			ualJndex = 0;
			ualJndex < VectorSize(hvData);
			ualJndex++
		){
			if (ualJndex == ualIndex) /* Skip if same value */
			{ continue; }

			lValueY = *(LPLONG)VectorAt(hvData, ualJndex);

			/* Check for Part 1 */
			if (2020 == lValueX + lValueY && FALSE == bFound1)
			{
				bFound1 = TRUE;
				llValueProduct = lValueX * lValueY;
				PrintFormat("Value 1: %ld\n", llValueProduct);
			}
			
			/* Third Loop */
			for (
				ualKndex = 0;
				ualKndex < VectorSize(hvData);
				ualKndex++
			){
				if (ualKndex == ualJndex || ualKndex == ualIndex) /* No Need */
				{ continue; }

				lValueZ = *(LPLONG)VectorAt(hvData, ualKndex);

				/* Check for Part 2 */
				if (2020 == lValueX + lValueY + lValueZ && FALSE == bFound2)
				{
					bFound2 = TRUE;
					llValueProduct = lValueX * lValueY * lValueZ;
					PrintFormat("Value 2: %ld\n", llValueProduct);
				}
			}

		}
	}

	/* Destroy Objects */
	DestroyVector(hvData);
	DestroyObject(hFile);

	return 0;
}