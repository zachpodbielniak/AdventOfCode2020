#     _       _                 _    ___   __  ____          _      ____   ___ ____   ___  
#    / \   __| |_   _____ _ __ | |_ / _ \ / _|/ ___|___   __| | ___|___ \ / _ \___ \ / _ \ 
#   / _ \ / _` \ \ / / _ \ '_ \| __| | | | |_| |   / _ \ / _` |/ _ \ __) | | | |__) | | | |
#  / ___ \ (_| |\ V /  __/ | | | |_| |_| |  _| |__| (_) | (_| |  __// __/| |_| / __/| |_| |
# /_/   \_\__,_| \_/ \___|_| |_|\__|\___/|_|  \____\___/ \__,_|\___|_____|\___/_____|\___/ 

# AdventOfCode2020 Challenge
# Copyright (C) 2020 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE


OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
MARCH = -march=native
MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
CC_FLAGS += $(MARCH)
CC_FLAGS += $(MTUNE)


ASM_FLAGS = -Wall



FILES = Day-01 Day-01-ASM Day-02 Day-03 Day-04




all:	bin $(FILES)

bin:
	mkdir -p bin/

clean: 	bin
	rm -rf bin/

Day-01: bin 
	$(CC) -o bin/day-01 Day-01/Main.c -lpodnet $(CC_FLAGS)

Day-01-ASM: bin
	$(ASM) -g -F dwarf -f elf64 -o bin/Day-01-ASM.o Day-01/Main.asm
	$(CC) -g -fPIE -o bin/day-01-asm bin/Day-01-ASM.o -lpodnet_d 
	rm bin/*.o

Day-02: bin 
	$(CC) -o bin/day-02 Day-02/Main.c -lpodnet $(CC_FLAGS)

Day-03: bin 
	$(CC) -o bin/day-03 Day-03/Main.c -lpodnet $(CC_FLAGS)

Day-04: bin 
	$(CC) -g -o bin/day-04 Day-04/Main.c -lpodnet_d