#include <PodNet/PodNet.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CContainers/CVector.h>
#include <PodNet/CString/CString.h>


#define INPUT_FILE "./Input/Day-02-00.txt"


LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hFile;
	HVECTOR hvData;
	CSTRING csBuffer[1024];
	LPSTR lpszBuffer;
	LPSTR lpszDup;
	LPSTR lpszLine;
	LPSTR lpszIterator;
	UARCHLONG ualIndex;
	LONG lLower;
	LONG lHigher;
	LONG lCount;
	LONG lSplitCount;
	LONG lValidCount1;
	LONG lValidCount2;
	DLPSTR dlpszLineSplit;
	DLPSTR dlpszLimitSplit;

	
	/* Open File */
	hFile = OpenFile(INPUT_FILE, FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == hFile)
	{ PostQuitMessage(1); }

	/* Create Vector */
	hvData = CreateVector(0x200, sizeof(LPSTR), 0);
	if (NULLPTR == hvData)
	{ PostQuitMessage(2); }

	/* For Address Of Access */
	lpszBuffer = (LPSTR)csBuffer;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	/* Set valid to 0 */
	lValidCount1 = 0;
	lValidCount2 = 0;

	/* Read the file line by line, and strdup the output into the vector for parsing later */
	while (0 < ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	)){
		lpszDup = strdup(lpszBuffer);
		if (NULLPTR == lpszDup)
		{ PostQuitMessage(3); }

		VectorPushBack(hvData, &lpszDup, 0);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){
		lpszLine = *(DLPSTR)VectorAt(hvData, ualIndex);

		/* No need for new line char */
		StringReplaceCharacter(lpszLine, '\n', '\0');

		/* Split the line by spaces */
		/* Limits in dlpszLineSplit[0], char in dlpszLineSplit[1], and password in dlpszLineSplit[2] */
		lSplitCount = StringSplit(
			lpszLine, 
			" ", 
			&dlpszLineSplit
		);

		if (3 != lSplitCount)
		{ PostQuitMessage(4); }

		/* Split the limits by the hyphen */
		lSplitCount = StringSplit(
			dlpszLineSplit[0],
			"-",
			&dlpszLimitSplit
		);

		/* Lower Limit in dlpszLimitSplit[0], Higher in dlpszLimitSplit[1] */
		StringScanFormat(dlpszLimitSplit[0], "%d", &lLower);
		StringScanFormat(dlpszLimitSplit[1], "%d", &lHigher);

		/* Iterate through the string, and count the occurences for part 1 */
		lCount = 0;
		lpszIterator = dlpszLineSplit[2];
		while (0 != *lpszIterator)
		{
			if (*(dlpszLineSplit[1]) == *lpszIterator)
			{ lCount++; }

			lpszIterator = (LPSTR)(LPVOID)((UARCHLONG)lpszIterator + 0x01U);
		}

		/* Part 1 Check */
		if (lCount >= lLower && lCount <= lHigher)
		{ lValidCount1++; }

		/* Part 2 Check */
		if (
			(*(dlpszLineSplit[1]) == dlpszLineSplit[2][lLower - 1] && *(dlpszLineSplit[1]) != dlpszLineSplit[2][lHigher - 1]) ||
			(*(dlpszLineSplit[1]) != dlpszLineSplit[2][lLower - 1] && *(dlpszLineSplit[1]) == dlpszLineSplit[2][lHigher - 1])
		){ lValidCount2++; }

		/* Free the split strings */
		DestroySplitString(dlpszLimitSplit);
		DestroySplitString(dlpszLineSplit);
	}

	/* Print out */
	PrintFormat("Valid Part 1: %d\n", lValidCount1);
	PrintFormat("Valid Part 2: %d\n", lValidCount2);

	/* Destroy Objects */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){ FreeMemory(*(DLPSTR)VectorAt(hvData, ualIndex)); }

	DestroyVector(hvData);
	DestroyObject(hFile);

	return 0;
}