#include <PodNet/PodNet.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CContainers/CVector.h>
#include <PodNet/CString/CString.h>


#define INPUT_FILE "./Input/Day-04-00.txt"


typedef struct __INPUT_VALID
{
	BOOL 		bHasByr;
	BOOL 		bHasIyr;
	BOOL 		bHasEyr;
	BOOL 		bHasHgt;
	BOOL 		bHasHcl;
	BOOL 		bHasEcl;
	BOOL 		bHasPid;
	BOOL 		bHasCid;
} INPUT_VALID;



LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		lplszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(lplszArgValues);

	HANDLE hFile;
	HVECTOR hvData;
	CSTRING csBuffer[1024];
	CSTRING csInput[2048];
	LPSTR lpszBuffer;
	LPSTR lpszDup;
	LPSTR lpszLine;
	UARCHLONG ualIndex;
	UARCHLONG ualSize;
	LONG lIndex;
	ARCHLONG alCount;
	UARCHLONG ualSpaces;
	UARCHLONG ualValues;
	DLPSTR dlpszSpace;
	DLPSTR dlpszValues;
	INPUT_VALID ivData;

	
	/* Open File */
	hFile = OpenFile(INPUT_FILE, FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == hFile)
	{ PostQuitMessage(1); }

	/* Create Vector */
	hvData = CreateVector(0x200, sizeof(LPSTR), 0);
	if (NULLPTR == hvData)
	{ PostQuitMessage(2); }

	/* Clear Data */
	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csInput, sizeof(csInput));
	lpszBuffer = (LPSTR)csBuffer;

	/* Set Variables */
	lIndex = 0;
	alCount = 0;

	/* Read the file line by line, and strdup the output into the vector for parsing later */
	while (0 < ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	)){
		/* Check for end of data stream */
		if ('\n' == *lpszBuffer)
		{
			/* Dup the Buffer */
			lpszDup = strdup(csInput);
			if (NULLPTR == lpszDup)
			{ PostQuitMessage(3); }

			VectorPushBack(hvData, &lpszDup, 0);
			ZeroMemory(csInput, sizeof(csInput));
			continue; 
		}

		else 
		{
			StringConcatenateSafe(
				csInput, 
				csBuffer,
				sizeof(csInput) - 1
			);
		}

		ZeroMemory(csBuffer, sizeof(csBuffer));
	}
	
	/* Part 1 */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){
		ZeroMemory(&ivData, sizeof(INPUT_VALID)); 

		lpszLine = *(DLPSTR)VectorAt(hvData, ualIndex);
		StringReplaceCharacter(lpszLine, '\n', ' ');

		lIndex = StringLength(lpszLine);
		while (' ' != lpszLine[lIndex])
		{
			lpszLine[lIndex] = '\0';
			lIndex--;
		}

		ualSpaces = StringSplit(
			lpszLine, 
			" ",
			&dlpszSpace
		);

		for (
			lIndex = 0;
			lIndex < ualSpaces;
			lIndex++
		){
			if (NULLPTR == dlpszSpace[lIndex] || dlpszSpace[lIndex][0] == '\0')
			{ continue; }

			ualValues = StringSplit(
				dlpszSpace[lIndex],
				":",
				&dlpszValues
			);

			if (2 != ualValues)
			{ continue; }

			if (0 == StringCompare("byr", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasByr = TRUE; }
			else if (0 == StringCompare("iyr", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasIyr = TRUE; }
			else if (0 == StringCompare("eyr", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasEyr = TRUE; }
			else if (0 == StringCompare("hgt", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasHgt = TRUE; }
			else if (0 == StringCompare("hcl", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasHcl = TRUE; }
			else if (0 == StringCompare("ecl", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasEcl = TRUE; }
			else if (0 == StringCompare("pid", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasPid = TRUE; }
			else if (0 == StringCompare("cid", dlpszValues[0]) && '\0' != dlpszValues[1][0] && 0 != StringCompare(dlpszValues[1], ""))
			{ ivData.bHasCid = TRUE; }

			if (
				TRUE == ivData.bHasByr &&
				TRUE == ivData.bHasIyr &&
				TRUE == ivData.bHasEyr &&
				TRUE == ivData.bHasHgt &&
				TRUE == ivData.bHasHcl &&
				TRUE == ivData.bHasEcl && 
				TRUE == ivData.bHasPid 
			){ alCount++; }

			DestroySplitString(dlpszValues);
		}

		DestroySplitString(dlpszSpace);
	}
	
	/* Part 2 */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){
		lpszLine = *(DLPSTR)VectorAt(hvData, ualIndex);
		
	}

	PrintFormat("Part 1: %hd\n", alCount);

	/* Destroy Objects */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){ FreeMemory(*(DLPSTR)VectorAt(hvData, ualIndex)); }

	DestroyVector(hvData);
	DestroyObject(hFile);

	return 0;
}