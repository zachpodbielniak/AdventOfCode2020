#include <PodNet/PodNet.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CContainers/CVector.h>
#include <PodNet/CString/CString.h>


#define INPUT_FILE "./Input/Day-03-00.txt"


LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		lplszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(lplszArgValues);

	HANDLE hFile;
	HVECTOR hvData;
	CSTRING csBuffer[1024];
	LPSTR lpszBuffer;
	LPSTR lpszDup;
	LPSTR lpszLine;
	UARCHLONG ualIndex;
	UARCHLONG ualSize;
	LONG lIndex;
	LONG lTree;
	LONG lClear;
	LONG lplIndex2[5];
	LONG lplTrees2[5];
	LONGLONG llPart2Ans;

	
	/* Open File */
	hFile = OpenFile(INPUT_FILE, FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == hFile)
	{ PostQuitMessage(1); }

	/* Create Vector */
	hvData = CreateVector(0x200, sizeof(LPSTR), 0);
	if (NULLPTR == hvData)
	{ PostQuitMessage(2); }

	/* Clear Data */
	ZeroMemory(csBuffer, sizeof(csBuffer));
	lpszBuffer = (LPSTR)csBuffer;

	/* Set Variables */
	lIndex = 0;
	lClear = 0;
	lTree = 0;
	ZeroMemory(lplIndex2, sizeof(LONG) * 5);
	ZeroMemory(lplTrees2, sizeof(LONG) * 5);

	/* Read the file line by line, and strdup the output into the vector for parsing later */
	while (0 < ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	)){
		/* No need for new line char */
		StringReplaceCharacter(lpszBuffer, '\n', '\0');

		/* The Lazy Way */
		ualSize = StringLength(lpszBuffer) * 256;
		lpszDup = LocalAlloc(ualSize + 1);
		if (NULLPTR == lpszDup)
		{ PostQuitMessage(3); }

		for (
			ualIndex = 0;
			ualIndex < 256;
			ualIndex++
		){ StringConcatenateSafe(lpszDup, lpszBuffer, ualSize); }

		VectorPushBack(hvData, &lpszDup, 0);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}
	
	/* Part 1 */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){
		lpszLine = *(DLPSTR)VectorAt(hvData, ualIndex);
		
		if ('#' == lpszLine[lIndex])
		{ lTree++; }

		else if ('.' == lpszLine[lIndex])
		{ lClear++; } 

		lIndex += 3; 
	}
	
	/* Part 2 */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){
		lpszLine = *(DLPSTR)VectorAt(hvData, ualIndex);
		
		if ('#' == lpszLine[lplIndex2[0]])
		{ lplTrees2[0]++ ;}
		if ('#' == lpszLine[lplIndex2[1]])
		{ lplTrees2[1]++ ;}
		if ('#' == lpszLine[lplIndex2[2]])
		{ lplTrees2[2]++ ;}
		if ('#' == lpszLine[lplIndex2[3]])
		{ lplTrees2[3]++ ;}

		lplIndex2[0]++;
		lplIndex2[1] += 3;
		lplIndex2[2] += 5;
		lplIndex2[3] += 7;

		if (0 == ualIndex % 2)
		{	
			if ('#' == lpszLine[lplIndex2[4]])
			{ lplTrees2[4]++; }

			lplIndex2[4]++;
		}
	}

	llPart2Ans = lplTrees2[0] * lplTrees2[1] * lplTrees2[2] * lplTrees2[3] * lplTrees2[4];

	PrintFormat("Part 1: ------\nTrees: %d\nClear: %d\n\n", lTree, lClear);
	PrintFormat("Part 2: %ld\n", llPart2Ans);

	/* Destroy Objects */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvData);
		ualIndex++
	){ FreeMemory(*(DLPSTR)VectorAt(hvData, ualIndex)); }

	DestroyVector(hvData);
	DestroyObject(hFile);

	return 0;
}